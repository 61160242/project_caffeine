import 'package:flutter/material.dart';

class TableWidget extends StatelessWidget {
  const TableWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Center(
      child: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.all(20),
            child: Table(
              defaultColumnWidth: FixedColumnWidth(120.0),
              border: TableBorder.all(
                  color: Colors.black, style: BorderStyle.solid, width: 2),
              children: [
                TableRow(
                  children: [
                    Column(children: [Text('No.',style: TextStyle(fontSize: 20.0),)]),
                    Column(children: [Text('Name',style: TextStyle(fontSize: 20.0))]),
                    Column(children: [Text('Caffeine',style: TextStyle(fontSize: 20.0))]),
                    Column(children: [Text('Delete',style: TextStyle(fontSize: 20.0))])
                  ],
                ),
                TableRow(
                  children: [
                    Column(children: [Text('1',style: TextStyle(fontSize: 20.0))]),
                    Column(),
                    Column(),
                    Column(
                      children: [
                        Icon(
                          Icons.delete,
                          color: Colors.black,
                          size: 24.0,
                        )
                      ],
                    )
                  ],
                ),
                TableRow(
                  children: [
                    Column(children: [Text('2',style: TextStyle(fontSize: 20.0))]),
                    Column(),
                    Column(),
                    Column(
                      children: [
                        Icon(
                          Icons.delete,
                          color: Colors.black,
                          size: 24.0,
                        )
                      ],
                    )
                  ],
                ),
                TableRow(
                  children: [
                    Column(children: [Text('3',style: TextStyle(fontSize: 20.0))]),
                    Column(),
                    Column(),
                    Column(
                      children: [
                        Icon(
                          Icons.delete,
                          color: Colors.black,
                          size: 24.0,
                        )
                      ],
                    )
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    ));
  }
}
