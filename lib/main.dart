import 'package:caffine_with_me/List.dart';
import 'package:caffine_with_me/sleep.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  static const appTitle = 'CAFFEINE WITH ME';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: appTitle,
      home: MyHomePage(title: appTitle),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState(title: title);
}

class _MyHomePageState extends State<MyHomePage> {
  _MyHomePageState({required this.title});
  final String title;
  Widget body = Center(
    child: Text('MyPage!'),
  );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF8D6E63),
        title: Center(
          child: Text('CAFFEINE WITH ME'),
        ),
      ),
      body: body,
      drawer: Drawer(
        child: ListView(
          children: [
            const DrawerHeader(
                decoration: BoxDecoration(
                  color: Color(0xFF8D6E63),
                ),
                child: Center(
                  child: Text('CAFFEINE WITH ME'),
                )),
                ListTile(
                  title: const Text('List'),
                  onTap: (){
                    setState(() {
                      body = TableWidget();
                    });
                    Navigator.pop(context);
                  },
                ),
                ListTile(
                  title: const Text('Sleep'),
                  onTap: (){
                    setState(() {
                      body = Sleep();
                    });
                    Navigator.pop(context);
                  },
                )
          ],
        ),
      ),
    );
  }
}
